<?php

/**
 * @file
 * This is the Drupal.ru custom module.
 */

use Drupal\comment\CommentInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Add Ban Link helper function.
 */
function _drucustom_add_ban_link($entity, &$links) {
  // Current user.
  $account = \Drupal::currentUser();
  // Node author.
  $uid = $entity->uid->getString();
  // Node or comment.
  $entity_type_id = $entity->getEntityTypeId();

  if (in_array('moderator', $account->getRoles()) || in_array('administrator', $account->getRoles())) {
    $link['dru-ban'] = [
      'title' => t('Ban user!'),
      'url' => Url::fromUserInput('/user/' . $uid . '/cancel'),
    ];
    $links[$entity_type_id]['#links']['dru-ban'] = $link['dru-ban'];
  }
}

/**
 * Add SpamBot Link helper function.
 */
function _drucustom_add_spambot_link($entity, &$links) {
  // Current user.
  $account = \Drupal::currentUser();
  // Node author.
  $uid = $entity->uid->getString();
  // Node or comment.
  $entity_type_id = $entity->getEntityTypeId();

  $moduleHandler = \Drupal::service('module_handler');

  if ($moduleHandler->moduleExists('spambot')) {
    if (in_array('moderator', $account->getRoles()) || in_array('administrator', $account->getRoles())) {
      $link['dru-spambot'] = [
        'title' => t('Spambot'),
        'url' => Url::fromUserInput('/user/' . $uid . '/spambot'),
      ];
      $links[$entity_type_id]['#links']['dru-spambot'] = $link['dru-spambot'];
    }
  }
}

/**
 * Implements hook_node_links_alter().
 */
function drucustom_node_links_alter(array &$links, NodeInterface $entity, array &$context) {
  _drucustom_add_ban_link($entity, $links);
  _drucustom_add_spambot_link($entity, $links);
}

/**
 * Implements hook_comment_links_alter().
 */
function drucustom_comment_links_alter(array &$links, CommentInterface $entity, array &$context) {
  _drucustom_add_ban_link($entity, $links);
  _drucustom_add_spambot_link($entity, $links);
}
